execute pathogen#infect()

" -, H, L, <space>, <cr>, and <bs>

let mapleader=" "

" move line down
nnoremap <leader>j ddp

" move line up
nnoremap <leader>k ddkP

nnoremap <leader>ev :vsplit $MYVIMRC<cr>
nnoremap <leader>sv :source $MYVIMRC<cr>

" jk for escape key, disables c-c, c-[
inoremap jk <esc>
inoremap <c-c> <nop>
inoremap <c-[> <nop>

" replaces : with ;
nnoremap ; :
nnoremap : <nop>

syntax on
filetype plugin indent on

" editor preferences
set colorcolumn=80
set nu
let g:ctrlp_working_path_mode = 'w'
let g:ctrlp_use_caching = 0

" indentation
set shiftwidth=4
set shiftround
set tabstop=4
set expandtab

" solarized
set background=dark
colorscheme solarized

py3file ~/.vim/vimrc.py

nnoremap Bd py3 delete_buffer()
