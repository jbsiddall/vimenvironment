import vim

def show_line():
    print(vim.current.line)

def delete_buffer():
    current_window = vim.current.window
    delbuf = vim.current.buffer
    if delbuf.name == '':
        print('no need to delete buffer, its empty')
        return
    other_buffers = [buf for buf in vim.buffers if buf is not delbuf]
    assert len(other_buffers) > 0
    replacement = other_buffers[0]

    for win in [win for win in vim.windows if win.buffer is delbuf]:
        vim.current.window = win
        vim.current.buffer = replacement

    vim.current.window = current_window
    vim.command("bd {}".format(delbuf.number))
